===WB SMS ===
Contributors: WB SMS
Tags: contact sms, form, contact form sms,sms, feedback, email, ajax, captcha, 
Requires at least: 3.5
Stable tag: 3.6
Tested :3.6

contact form SMS plugin. Sending sms through contact form.

== Description ==

Transmit sms contact : contact to administartor site through sms

== Installation ==

1. Upload the entire `receive_sms_enquery` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Receive SMS Enquery' menu in your WordPress admin panel.


== Development==
version 1.0 
 - basic code development
