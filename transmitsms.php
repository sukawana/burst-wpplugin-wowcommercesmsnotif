<?php
/*
Plugin Name: Wowcommerce Receive SMS Enquiry
Plugin URI: https://burst.WBms.com/
Description: Sending notification sms to admin though Burst SMS.
Version: 1.0
Author: WB SMS >> Adi Sukawana
Author URI: https://burst.WBms.com/
*/
//if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    // Put your plugin code here

define( 'WB_VERSION', '1.0' );

define( 'WB_REQUIRED_WP_VERSION', '3.5' );

if ( ! defined( 'WB_PLUGIN_BASENAME' ) )
	define( 'WB_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );

if ( ! defined( 'WB_PLUGIN_NAME' ) )
	define( 'WB_PLUGIN_NAME', trim( dirname( WB_PLUGIN_BASENAME ), '/' ) );

if ( ! defined( 'WB_PLUGIN_DIR' ) )
	define( 'WB_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );

if ( ! defined( 'WB_PLUGIN_URL' ) )
	define( 'WB_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );

if ( ! defined( 'WB_PLUGIN_MODULES_DIR' ) )
	define( 'WB_PLUGIN_MODULES_DIR', WB_PLUGIN_DIR . '/modules' );

if ( ! defined( 'WB_LOAD_JS' ) )
	define( 'WB_LOAD_JS', true );

if ( ! defined( 'WB_LOAD_CSS' ) )
	define( 'WB_LOAD_CSS', true );

if ( ! defined( 'WB_AUTOP' ) )
	define( 'WB_AUTOP', true );

if ( ! defined( 'WB_USE_PIPE' ) )
	define( 'WB_USE_PIPE', true );
if(! defined('Recaptcha_Public_key'))
    define('Recaptcha_Public_key',"6LcUGOcSAAAAAADS1N4zhsOGAsRiJVL0SWI3x2or");
if(! defined('Recaptcha_Private_key'))
    define('Recaptcha_Private_key',"6LcUGOcSAAAAAMUIk2f32SQVExfcH6VFS6jLWJCM");
if(!defined('WB_orderRecivedMsg'))
    define('WB_orderRecivedMsg','Order has been received No [[order_number]], Date: [[order_date]],Total: [[order_total]], PAYMENT METHOD: [[order_payment_method]], Name:[[order_billing_first_name]] [[order_billing_last_name]], phone: [[order_billing_phone]], email: [[order_billing_email]]'. "\r\n".'Thank you');
  if(!defined('WB_failMsg'))
    define('WB_failMsg',"Oops! Sorry, looks like we have run into some problems. Please try again later, or contact via email");
if(!defined('WB_successVerify'))
    define('WB_successVerify',"Your key has been verified successfully");
if(!defined('WB_failVerify'))
    define('WB_failVerify',"Sorry..api key and secret you entered  still invalid");
if(!defined('WB_TemplateMsg'))
    define('WB_TemplateMsg','Website Enquiry From: [NAME]'."\n".'[MESSAGE]');  
    
$wpOption = 'WBSmsSettings';  
require_once WB_PLUGIN_DIR . '/settings.php';
?>
