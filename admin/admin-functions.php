<?php
function WB_admin_menu() {
	add_submenu_page('woocommerce', __('Burst SMS Notifications', 'burst_sms'),  __('Burst SMS Notifications', 'burst_sms') , 'manage_woocommerce', 'WBSMSC_options', 'WBSMSC_options');

    //add_menu_page( 'Receive SMS Enquiry', 'Receive SMS Enquiry', 'manage_options', 'setting contact form', 'WBSMSC_options' );
}

function WBSMSC_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['WB_hidden']) && $_POST['WB_hidden'] == "Y"){
            WBSMSC_handleSubmit();
        }
        echo WBSMSC_settingForm();
}
function WBSMSC_settingForm(){
    $WBSms = unserialize(stripslashes(get_option('WBSmsSettings')));
    $burstSmsApiKey  = base64_decode($WBSms['apikey']);
    $burstSmsApiSecret  = base64_decode($WBSms['apisecret']);
    $burstSmsAdminNumber = base64_decode($WBSms['reciver_number']);
    //$burstSmsownerCostum = trim($WBSms['ownerCostum']);
    $burstSmsreceivedCustom = trim($WBSms['receivedCustom']);
    $burstSmsprocessingCustom = trim($WBSms['processingCustom']);
    $burstSmscompletedCustom = trim($WBSms['completedCustom']);
    $burstSmsnotesCustom = trim($WBSms['notesCustom']);
    $burstSmsList = $WBSms['list_id'];
    if(empty($burstSmsreceivedCustom))$burstSmsreceivedCustom = WB_orderRecivedMsg;
     ob_start();  
  
?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('[rel="Transmitparamdata"]').css('display','none');
        renderlist();
        required = ["WB_apikey", "WB_apisecret", "WB_adminNumber","WB_submitLabel","WB_success","WB_fail"];
        errornotice = jQuery("#error");
        emptyerror = "Please fill out this field.";
        jQuery("#WBform").submit(function(){	
	//Validate required fields
		for (i=0;i<required.length;i++) {
			var input = jQuery('#'+required[i]);
			if ((input.val() == "") || (input.val() == emptyerror)) {
				input.addClass("needsfilled");
				input.val(emptyerror);
				errornotice.fadeIn(750);
			} else {
				input.removeClass("needsfilled");
			}
		}
		//if any inputs on the page have the class 'needsfilled' the form will not submit
		if (jQuery(":input").hasClass("needsfilled")) {
                   return false;
		} else {
			errornotice.hide();
			return true;
		}
         });
         // Clears any fields in the form when the user clicks on them
	jQuery(":input").focus(function(){		
	   if (jQuery(this).hasClass("needsfilled") ) {
			jQuery(this).val("");
			jQuery(this).removeClass("needsfilled");
	   }
	});
     });
     
     function renderlist(){
         var apikey = jQuery('#WB_apikey').val();
         var apisecret = jQuery('#WB_apisecret').val();
          if(apikey != "" && apisecret != ""){
            jQuery("#msgVerify").fadeIn('fast');
            jQuery.ajax({
                   url: '<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>',
                   type:'POST',
                   data:'getlist=Y&apikey=' + apikey + '&secret=' + apisecret + '&selected=' + <?php echo empty($burstSmsList)?"'N'":$burstSmsList ?>,
                   success: function(result){
                           if(result == 'error'){
                                jQuery("#msgVerify").css("color","red");
                                jQuery("#msgVerify").html("<?php echo WB_failVerify ?>");
                                jQuery('[rel="Transmitparamdata"]').css('display','none');
                           }else{
                                jQuery('#WB_addToList').html(result);
                                jQuery("#msgVerify").css("color","green");
                                jQuery("#msgVerify").html("<?php echo WB_successVerify ?>");
                                jQuery('[rel="Transmitparamdata"]').css('display','block');
                           } 
                        }
		});
               }
      return false;
    }
 </script>
<style>
    #error {
	color:red;
	font-size:10px;
	display:none;
    }
    .needsfilled {
	
	color:red !important;
        border: 1px solid red !important; 
    }
    form ul{list-style-type: none;}
        form ul li{clear: both;height: auto;padding-bottom: 30px;position: relative;}
        .clearfix:after {clear: both;content: ".";display: block;height: 0;margin-bottom: -17px;visibility: hidden;}
        .clearfix {display: block;}
    label {padding-left: 0px;width: 120px;color: #3C3C3C;float: left;text-align:left;}
    input[type="text"], select, textarea, .textarea {background: none repeat scroll 0 0 #F0EFE9;border: 1px solid #938F77;color:#666;float: left;outline: medium none;padding: 4px;width:550px;}
    textarea{height:120px;}
    form em{
        font-size: 11px;
        margin-left: 120px;
    }
    
</style>
<div id="post-body" class="metabox-holder columns-1">
<div class="wrap"> 
    <div id="icon-options-general" class="icon32">
        <br> </div>
<h2> <?php echo  __( 'Receive SMS Enquiry', 'WB_trdom' );?> </h2><br>

    <div id="postbox-container-2" class="postbox-container">
        <div id="normal-sortables" class="meta-box-sortables ui-sortable">
            <div id="revisionsdiv" class="postbox ">
           <!-- <div class="handlediv" title="Click to toggle"><br></div> -->
                <h3 class="hndle"><span>
                        <?php    echo "<span>" . __( 'Settings', 'WB_trdom' ) . "</span></h3>"; ?>
            <div class="inside">

            <div style="width:96%; padding:10px">
                <form name="WBform" id="WBform" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">  
                    <input type="hidden" name="WB_hidden" value="Y">  
                    
                    <div style=" width:700px">
                        <ul style=" width:700px">
                            <li class="clearfix" style="margin-bottom:-7px !important"> <label for="WB_apikey"><?php _e("API Key : " ); ?> </label><input type="text" name="WB_apikey" id="WB_apikey" value="<?php echo $burstSmsApiKey; ?>" >
                        </li>
                        <li class="clearfix" style="margin-bottom:-7px !important"><label for="WB_apisecret"><?php _e("API Secret : " ); ?></label><input type="text" name="WB_apisecret"  id="WB_apisecret" value="<?php echo $burstSmsApiSecret; ?>" >
                            <em> Get these details from the API settings section of your account.</em>
                        </li>
                         <li class="clearfix">
                             <label for="verify">&nbsp;</label>
                               <input id="verify" class="button button-primary button-large" type="button" onclick="renderlist();" accesskey="p" value="Verify key" name="verify">
                               <em id="msgVerify" style="margin-left:10px !important;display:none"><img src="<?php echo WB_PLUGIN_URL ?>/images/loading.gif" title="still loading" > </em>
                         </li>
                       <li class="clearfix" rel="Transmitparamdata"><label for="WB_adminNumber"><?php _e("Moblie Number : " ); ?></label><input type="text" name="WB_adminNumber" id="WB_adminNumber" value="<?php echo $burstSmsAdminNumber; ?>">
                           <em> The mobile number you wish to receive messages on in international format eg. +614XXXXXXXX +447XXXXXXXX  </em>
                       </li>
                         <li class="clearfix" rel="Transmitparamdata"><label for="WB_addtolits"><?php _e("Add to list: " ); ?></label><select name="WB_addToList" id="WB_addToList"> </select>
                        <em> List ID can be found just before the list name when viewing the list.</em>
                     </li>
                     <!--
                       <li class="clearfix" rel="Transmitparamdata"><label for="WB_ownerCustom"><?php _e("Owner costum message : " ); ?></label><textarea name="WB_ownerCostum" id="WB_ownerCostum"><?=$burstSmsownerCostum?></textarea>
                        <em> You can use the variables : [NAME] and [MESSAGE]  </em>
                     </li> -->
                       <li class="clearfix" rel="Transmitparamdata"><label for="WB_receivedCustom"><?php _e("Order received custom message : " ); ?></label><textarea type="text" name="WB_receivedCustom"  id="WB_receivedCustom" ><?=$burstSmsreceivedCustom?></textarea>
                            <em> This message would sent to admin.</em>
                   
                       </li>
                        <li class="clearfix" rel="Transmitparamdata"><label for="WB_processingCustom"><?php _e("Order processing custom message : " ); ?></label><textarea type="text" name="WB_processingCustom"  id="WB_processingCustom"> <?=$burstSmsprocessingCustom?></textarea>
                             <em> This message would  sent to customer.</em>
                        </li>
                       <li class="clearfix" rel="Transmitparamdata"><label for="WB_completedCustom"><?php _e("Order completed custom message : " ); ?></label><textarea type="text" name="WB_completedCustom"  id="WB_completedCustom"><?=$burstSmscompletedCustom?></textarea>
                        <em> This message would send to customer when his order has complate.</em></li>
                       <li class="clearfix" rel="Transmitparamdata"><label for="WB_notesCustom"><?php _e("Notes custom message : " ); ?></label><textarea name="WB_notesCustom"  id="WB_notesCustom"><?=$burstSmsnotesCustom?></textarea> 
                         <em> This note message would sent to customer.</em>
                        </li>
                        <li class="clearfix" rel="Transmitparamdata"> <label for="publish">&nbsp;</label>
                            <input id="publish" class="button button-primary button-large" type="submit" accesskey="p" value="Update Options" name="publish">

                      </li>
                      
                        </div>  
                </form>  
                </div>
                </div>
        </div>

    </div>
    </div>

     <div id="postbox-container-2" class="postbox-container" style="margin-left:20px">
        <div id="normal-sortables" class="meta-box-sortables ui-sortable">
            <div id="revisionsdiv" class="postbox ">
                 <h3 class="hndle"><span>
                        Avalaible Shortcode </span> </h3>
                    <div class="inside">
                        <ul>
                            <li style="height: 339px;list-style:bullet;margin-left: 10px;margin-bottom: 10px; width: 340px;">
                             <div style="float: left; width: 105px; padding: 4px;background-color: #D7FFF4; margin: 4px;">[[order_number]]</div>
                            <div style="float: left; width: 90px; padding: 4px;background-color: #D7FFF4;margin: 4px;">[[order_date]]</div>
                            <div style="float: left; width: 90px; padding: 4px;background-color: #D7FFF4;margin: 4px;">[[order_total]]</div>
                            <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;">[[order_payment_method]]</div>
                           <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_first_name]]</div>
                             <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;">[[order_billing_last_name]]</div>
                            <div style="float: left; width: 135px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_phone]]</div>
                            <div style="float: left; width: 130px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_email]]</div>
                             <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_company]]</div>
                             <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;">  [[order_billing_company]]</div>
                              <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_address_1]]</div>
                              <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_address_2]]</div>
                              <div style="float: left; width: 130px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_city]]</div>
                            <div style="float: left; width: 130px; padding: 4px;background-color: #D7FFF4;margin: 4px;">   [[order_billing_state]]</div>
                            <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_postcode]]</div>
                              <div style="float: left; width: 160px; padding: 4px;background-color: #D7FFF4;margin: 4px;"> [[order_billing_country]]</div>
                             </li>
                        </ul>
                    </div>
                 <h3 class="hndle"><span>  How to use </span> </h3>
                    <div class="inside">
                          bla bla bla..
                      <!--       <span style="background-color:#ffee92;padding: 4px;margin-top:4px">echo do_shortcode('[[WBSMSContact]]'); </span> </li> -->
                </div>
            </div>
            </div>
        </div>                
            
</div> 
</div>
    <?php
    $FORM = ob_get_contents();
    ob_end_clean();
    
   return $FORM;
    
}
function  WBSMSC_handleSubmit(){
    $apikey = base64_encode(trim($_POST['WB_apikey']));
    $apisecret = base64_encode(trim($_POST['WB_apisecret']));
    $recivernumber = base64_encode(trim($_POST['WB_adminNumber']));
    $ownerCostum = empty($_POST['WB_ownerCostum'])?'':$_POST['WB_ownerCostum'];
    $receivedCustom = trim($_POST['WB_receivedCustom']);
    $processingCustom = trim($_POST['WB_processingCustom']);
    $completedCustom = trim($_POST['WB_completedCustom']);
    $notesCustom = trim($_POST['WB_notesCustom']);
    $listId = $_POST['WB_addToList'];
    $arrSEtting = array('apikey'=>$apikey,'apisecret'=>$apisecret,'reciver_number' =>$recivernumber,
                    'ownerCostum'=>$ownerCostum,'receivedCustom'=> $receivedCustom, 'processingCustom' => $processingCustom,
                    'completedCustom' => $completedCustom, 'notesCustom'=>$notesCustom,'list_id'=>$listId);
                    
    update_option( 'WBSmsSettings', addslashes(serialize($arrSEtting)));
}

?>