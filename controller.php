<?php
require_once(WB_PLUGIN_DIR.'/recaptcha-php-1.11/recaptchalib.php');

if(isset($_POST['BurstSMSsendToken']) && $_POST['BurstSMSsendToken'] == 'Y'){
    //check captcha
    if ($_POST["recaptcha_response_field"]) {
        $resp = recaptcha_check_answer (Recaptcha_Private_key,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);

        if ($resp->is_valid) {
            require_once WB_PLUGIN_DIR .'/APIClient2.php';
            $WBSms = unserialize(stripslashes(get_option('WBSmsSettings')));
            $burstSmsApiKey  = base64_decode($WBSms['apikey']);
            $burstSmsApiSecret  = base64_decode($WBSms['apisecret']);
            $mobileIntFormat = base64_decode($WBSms['reciver_number']);
            $burstSmsSuccessMsg = $WBSms['success_message'];
            $burstSmsfailMsg = $WBSms['fail_message'];
            $burstSmsMsgTemplate = $WBSms['message_template'];
            $burstSmsList = $WBSms['list_id'];
            $WBmsAPI = new WBmsAPI($burstSmsApiKey, $burstSmsApiSecret);
            $body = str_replace(array('[NAME]','[MESSAGE]'),array($_POST['TrSMSname'],$_POST['TrSMSmsg']),$burstSmsMsgTemplate); 
            $caller_id = $_POST['TrSMSphone'];
            $result=$WBmsAPI->sendSms($body, $mobileIntFormat,$caller_id);
            if($result->error->code=='SUCCESS'){ 
                //add to list
                $customFields=array('description'=>'added from wordpress plugin : '.get_bloginfo('wpurl'));
                $result=$WBmsAPI->addToList($burstSmsList, $caller_id, $_POST['TrSMSname'], '', $customFields);
                echo "<p class=\"success\">".$burstSmsSuccessMsg."</p>";
            }else{
                 echo "<p class=\"error\">".$burstSmsfailMsg."</p>";
            }
           
            
            
            
        } else {
                    echo "<p class=\"error\">Sorry..captcha code you entered  still invalid, please try again later..</p>";
        }
    }else {
         echo "<p class=\"error\">Sorry..captcha code you entered  still invalid, please try again later..</p>";
    }
    exit();
}
?>
