<?php
function WBSMSC_registerShortcodes(){
    global $woocommerce, $wp, $bsMsgResult;
    require_once WB_PLUGIN_DIR .'/APIClient2.php';
    if(isset($_GET['order-received']) && !empty($_GET['order-received'])){ //order received
        $order_id = '';
        $order_key = '';
        $order= '';
        $order_id  = apply_filters( 'woocommerce_thankyou_order_id', empty( $_GET['order-received'] ) ? '' : wc_clean( $_GET['order-received'] ) );
        $order_key = apply_filters( 'woocommerce_thankyou_order_key', empty( $_GET['key'] ) ? '' : wc_clean( $_GET['key'] ) );
      	if ( $order_id > 0 ) {
            $order = new WC_Order( $order_id );
            if ( $order->order_key != $order_key )
    			unset( $order );
        }
        
        $WBSms = unserialize(stripslashes(get_option('WBSmsSettings')));
        $burstSmsApiKey  = base64_decode($WBSms['apikey']);
        $burstSmsApiSecret  = base64_decode($WBSms['apisecret']);
        $mobileIntFormat = base64_decode($WBSms['reciver_number']);
        $arrShortcode = array('[[order_number]]','[[order_date]]','[[order_total]]','[[order_payment_method]]','[[order_billing_first_name]]','[[order_billing_last_name]]','[[order_billing_phone]]','[[order_billing_email]]','[[order_billing_company]]','[[order_billing_address_1]]','[[order_billing_address_2]]','[[order_billing_city]]','[[order_billing_state]]','[[order_billing_postcode]]','[[order_billing_country]]');
        $arryReplaceShortcode= array($order->get_order_number(),date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ), strip_tags($order->get_formatted_order_total()),
                                $order->payment_method_title,$order->billing_first_name,$order->billing_last_name,$order->billing_phone,$order->billing_email);
        
        $textMessage = str_replace($arrShortcode,$arryReplaceShortcode,$WBSms['receivedCustom']);
       /*
        $burstSmsSuccessMsg = $WBSms['success_message'];
        $burstSmsfailMsg = $WBSms['fail_message'];
        $burstSmsMsgTemplate = $WBSms['message_template'];
        */
        $burstSmsList = $WBSms['list_id'];
        $WBmsAPI = new transmitsmsAPI($burstSmsApiKey, $burstSmsApiSecret);
        //$caller_id = 'REPLY-NUMBER';
        $caller_id = trim($order->billing_phone);
       
        $result=$WBmsAPI->sendSms($textMessage, $mobileIntFormat,$caller_id);
        if($result->error->code=='SUCCESS'){ 
            //add to list
            $customFields=array('description'=>'added from wordpress plugin : '.get_bloginfo('wpurl'));
            $result=$WBmsAPI->addToList($burstSmsList, $caller_id, $order->billing_first_name, $order->billing_last_name, $customFields);
           // $bsMsgResult = "<p class=\"success\">me</p>";
        }else{
           // $bsMsgResult =  "<p class=\"error\">Fail : ".$result->error->description."</p>";
        } 
        if (!is_admin()) { //load jquery and css
            add_action("wp_enqueue_scripts", "my_enqueue", 11);
        }
        add_action('wp_footer', 'dispalyMsgOut');
       }
}

function dispalyMsgOut() {
    global $bsMsgResult;
    ob_start();
    ?>
   <script type="text/javascript">
    jQuery(document).ready(function(){
                jQuery('.entry-header').prepend('aa <?=$bsMsgResult ?>');
    });
    </script>
    <?PHP
    $jsscript = ob_get_contents();
    ob_end_clean();
    echo $jsscript;
    
}
function my_enqueue() {
   wp_enqueue_style('my-script-slug', WB_PLUGIN_URL. '/style.css');
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://code.jquery.com/jquery-latest.min.js", false, null);
   wp_enqueue_script('jquery');
}

?>
