<?php
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    // Put your plugin code here
    if ( is_admin() )
	   require_once WB_PLUGIN_DIR . '/admin/admin.php';   
    else{
        require_once WB_PLUGIN_DIR . '/functions.php';
        add_action( 'init', 'WBSMSC_registerShortcodes');
    }
}else{
    echo "wowcommerce not active yet !!!";
}

register_uninstall_hook(WB_PLUGIN_DIR . '/uninstall.php', 'delete_plugin');
?>
